module Program(T, parse, fromString, toString, exec) where
import Parser hiding (T)
import qualified Statement
import qualified Dictionary
import Prelude hiding (return, fail)
newtype T = Program [Statement.T]
instance Parse T where
  parse = iter Statement.parse >-> Program 
  toString (Program []) = ""
  toString (Program (stmt:stmts)) = Statement.toString stmt ++ toString (Program stmts)
             
exec (Program prog) = Statement.exec prog Dictionary.empty