module Statement(T, parse, toString, fromString, exec) where
import Prelude hiding (return, fail)
import Parser hiding (T)
import qualified Dictionary
import qualified Expr
type T = Statement
data Statement =
    	   Assignment String Expr.T 
	     | Skip
	     | BeginEnd [Statement]
	     | If Expr.T Statement Statement
	     | While Expr.T Statement
	     | Read String
	     | Write Expr.T
    deriving Show

assignment = word #- accept ":=" # Expr.parse #- require ";" >-> buildAss
buildAss (v, e) = Assignment v e

skip = accept "skip" -# require ";" >-> buildSkip
buildSkip e = Skip

beginEnd = accept "begin" -# (iter stmt) #- require "end" >-> buildBeginEnd
buildBeginEnd = BeginEnd

ifElse = accept "if" -# Expr.parse #- accept "then" # stmt #- require "else" # stmt >-> buildIfElse
buildIfElse (t, e)= If (fst t) (snd t) e

while = accept "while" -# Expr.parse #- require "do" # stmt >-> buildWhile
buildWhile (e,s) = While e s

readW = accept "read" -# word #- require ";" >-> buildRead
buildRead = Read

write = accept "write" -# Expr.parse #- require ";">-> buildWrite
buildWrite = Write

stmt = assignment ! skip ! beginEnd ! ifElse ! while !readW ! write

exec :: [T] -> Dictionary.T String Integer -> [Integer] -> [Integer]
exec [] dict input = []
exec (Assignment str exp: stmts) dict input = exec stmts (Dictionary.insert (str, Expr.value exp dict) dict) input --måste man ta bort gammal entry eller är dict intellignt?
exec (Skip: stmts) dict input =  exec stmts dict input
--exec (BeginEnd (st:sts): stmts) dict input = exec (BeginEnd sts:(st:stmts)) dict input
--exec (BeginEnd []: stmts) dict input = exec stmts dict input
exec (BeginEnd sts:stmts ) dict input = exec (sts ++ stmts) dict input
exec (If cond thenStmts elseStmts: stmts) dict input = 
    if (Expr.value cond dict)>0 
    then exec (thenStmts: stmts) dict input
    else exec (elseStmts: stmts) dict input

exec (While cond stmt : stmts) dict input = 
	if (Expr.value cond dict)>0
	then exec (stmt : While cond stmt : stmts) dict input
	else exec stmts dict input

exec (Read str: stmts) dict input = exec stmts (Dictionary.insert (str,(head input)) dict) $ tail input
exec (Write exp: stmts) dict input = Expr.value exp dict : exec stmts dict input

instance Parse Statement where
  parse = stmt
  toString (Assignment var val) = var ++ " := " ++ Expr.toString val ++ "\n"
  toString (Skip) = "skip;\n"
  toString (BeginEnd stmts) = "begin\n" ++ (concat $ map toString stmts) ++ "end\n"
  toString (If exp t e) = "if " ++ Expr.toString exp ++ " then\n" ++ toString t ++ "else\n" ++ toString e
  toString (While exp stmt) = "while " ++ Expr.toString exp ++ " do\n" ++ toString stmt
  toString (Read str) = "read " ++ str ++ "\n"
  toString (Write exp) = "write " ++ Expr.toString exp ++ "\n"
